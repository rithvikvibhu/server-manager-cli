import {Command, flags} from '@oclif/command'
const sc = require('windows-service-controller');

const ALL_SERVICES = ['Radarr', 'Lidarr', 'NzbDrone', 'Jackett', 'PlexService']

export default class Start extends Command {
  static description = 'Start services (all or selective)'

  static flags = {
    help: flags.help({char: 'h'}),
    restart: flags.boolean({char: 'r'})
  }

  static args = [
    {
      name: 'services',
      required: false,
      description: 'Comma separated list of services',
      hidden: false,
      parse: input => input.split(','),
      default: ALL_SERVICES
    }
  ]

  async run() {
    const {argv, flags} = this.parse(Start)
    
    var services = argv.length ? argv[0] : ALL_SERVICES;
    sc.start(services).then(res => {
      console.log('Started successfully.');
    }).catch(err => console.error(err));
  }
}
