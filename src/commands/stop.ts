import {Command, flags} from '@oclif/command'
const sc = require('windows-service-controller');

const ALL_SERVICES = ['Radarr', 'Lidarr', 'NzbDrone', 'Jackett', 'PlexService']

export default class Stop extends Command {
  static description = 'Start services (all or selective)'

  static flags = {
    help: flags.help({char: 'h'})
  }

  static args = [
    {
      name: 'services',
      required: false,
      description: 'Comma separated list of services',
      hidden: false,
      parse: input => input.split(','),
      default: ALL_SERVICES
    }
  ]

  async run() {
    const {argv, flags} = this.parse(Stop)

    var services = argv.length ? argv[0] : ALL_SERVICES;
    sc.stop(services).then(res => {
      console.log('Stopped successfully.');
    }).catch(err => console.error(err));
  }
}
