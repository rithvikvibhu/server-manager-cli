import {Command, flags} from '@oclif/command'
const sc = require('windows-service-controller');

const ALL_SERVICES = ['Radarr', 'Lidarr', 'NzbDrone', 'Jackett', 'PlexService']

export default class Status extends Command {
  static description = 'Displays status of all services'

  static flags = {
    help: flags.help({char: 'h'}),
  }
  
  static args = [
    {
      name: 'services',
      required: false,
      description: 'Comma separated list of services',
      hidden: false,
      parse: input => input.split(','),
      default: ALL_SERVICES
    }
  ]

  async run() {
    const {argv, flags} = this.parse(Status)

    var services = argv.length ? argv[0] : ALL_SERVICES;
    // this.log('Querying status for', services);
    var filteredServices = await sc.query({state: 'all'}).then(res => {
      return res.filter(service => services.includes(service.name))
    });
    
    console.log('\nService        ', 'Status\n------------------------');
    for (var service of filteredServices) {
      console.log(service.name.padEnd(15, '.'), service.state.name);
    }
  }
}
