server-manager-cli
==================



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g server-manager-cli
$ server-manager-cli COMMAND
running command...
$ server-manager-cli (-v|--version|version)
server-manager-cli/0.0.0 win32-x64 node-v11.5.0
$ server-manager-cli --help [COMMAND]
USAGE
  $ server-manager-cli COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`server-manager-cli help [COMMAND]`](#server-manager-cli-help-command)
* [`server-manager-cli start [SERVICES]`](#server-manager-cli-start-services)
* [`server-manager-cli status [SERVICES]`](#server-manager-cli-status-services)
* [`server-manager-cli stop [SERVICES]`](#server-manager-cli-stop-services)


## `server-manager-cli help [COMMAND]`

display help for server-manager-cli

```
USAGE
  $ server-manager-cli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.4/src/commands/help.ts)_

## `server-manager-cli start [SERVICES]`

Start services (all or selective)

```
USAGE
  $ server-manager-cli start [SERVICES]

ARGUMENTS
  SERVICES  [default: Radarr,Lidarr,NzbDrone,Jackett,PlexService] Comma separated list of services

OPTIONS
  -h, --help     show CLI help
  -r, --restart
```

_See code: [src/commands/start.ts](src/commands/start.ts)_

## `server-manager-cli status [SERVICES]`

Displays status of all services

```
USAGE
  $ server-manager-cli status [SERVICES]

ARGUMENTS
  SERVICES  [default: Radarr,Lidarr,NzbDrone,Jackett,PlexService] Comma separated list of services

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/status.ts](src/commands/status.ts)_

## `server-manager-cli stop [SERVICES]`

Start services (all or selective)

```
USAGE
  $ server-manager-cli stop [SERVICES]

ARGUMENTS
  SERVICES  [default: Radarr,Lidarr,NzbDrone,Jackett,PlexService] Comma separated list of services

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/stop.ts](src/commands/stop.ts)_
<!-- commandsstop -->
